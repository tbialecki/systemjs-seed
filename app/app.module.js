//TODO configure modernizr 3.x
//import 'modernizr';
import 'respond';

import angular from 'angular';
import 'angular-route';
import 'tmp/app/app.templates.js';
import mainModule from './main/main.module.js';
import component1Module from './component-1/component-1.module.js';
import component2Module from './component-2/component-2.module.js';

let appModule = angular.module('app', ['ngRoute', mainModule.name, component1Module.name, component2Module.name])
    .config(function ($locationProvider) {
        $locationProvider.html5Mode(false);
    });
export default appModule;
