import angular from 'angular';
import AppCntl from './app/app.controller.js';
import 'angular-route';
export default angular.module('app.main', ['ngRoute'])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {redirectTo: '/main/welcome'})
            .when('/main/welcome', {templateUrl: 'main/welcome/welcome.html'})
            .otherwise({templateUrl: 'main/page-not-found/page-not-found.html'});
    })
    .controller('AppCntl',AppCntl);
