import angular from 'angular';
import 'angular-mocks';


describe('\'app\' module', function () {
    var locationProvider;

    beforeEach(function () {
        angular.mock.module('ng', function ($locationProvider) {
            locationProvider = $locationProvider;
            spyOn(locationProvider, 'html5Mode').and.callThrough();
        });
        angular.mock.module('app');
    });

    // This is necessary to trigger loading the modules above; use it to inject services once they are needed
    beforeEach(angular.mock.inject());

    it('sets the \'Hashbang\' mode', function () {
        expect(locationProvider.html5Mode).toHaveBeenCalledWith(false);
    });
});
