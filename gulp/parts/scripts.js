/*global config, $*/
'use strict';
var gulp = require('gulp');
var Builder = require('systemjs-builder');

gulp.task('scripts',['ngTemplates'], function (done) {
    //var config = require('../../system.config.js');
    var builder = new Builder('/', './system.config.js');
    builder.bundle('app/app.module.js', 'dist/app/app.module.js')
        .then(function(){
            done();
        })
        .catch(function(ex) {
            done(new Error(ex));
        });
    //return gulp.src(config.scripts.src())
    //    .pipe(webpack({
    //        resolve: {
    //            extensions: ['', '.ts', '.js']
    //        },
    //        // Source maps support (or 'inline-source-map' also works)
    //        devtool: 'inline-source-map',
    //        // Add loader for .ts files.
    //        module: {
    //            loaders: [
    //                {
    //                    test: /\.ts$/,
    //                    loader: 'awesome-typescript-loader'
    //                }
    //            ]
    //        },
    //        output: {
    //            filename: config.scripts.bundle()
    //        }
    //    }))
    //    .pipe(gulp.dest(config.output()))
    //    .pipe($.size());
});
