import angular from 'angular';
import 'angular-route';

export default angular.module('app.component2', ['ngRoute'])
    .config(function ($routeProvider) {
        $routeProvider.when('/component-2/dialog-b', {templateUrl: 'component-2/dialog-b/dialog-b.html'});
    });
