import angular from 'angular';
import 'angular-route';

export default angular.module('app.component1', ['ngRoute'])
    .config(function ($routeProvider) {
        $routeProvider.when('/component-1/dialog-a', {templateUrl: 'component-1/dialog-a/dialog-a.html'});
    });


